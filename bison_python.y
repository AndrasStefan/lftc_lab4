%{
    #include <string.h>
	void yyerror(char []);
    int yylex();
    int yylineno;
	#include <stdio.h>
	#include <stdlib.h>	

	#define YYDEBUG 1
    #define ANSI_COLOR_RED     "\x1b[31;1m"
    #define ANSI_COLOR_GREEN   "\x1b[32;1m"
    #define ANSI_COLOR_RESET   "\x1b[0m"
%}

%union{
    int l_val;
    char *p_val;
}

%token <p_val> CONST_INT
%token <p_val> CONST_REAL
%token NEWLINE
%token ATRIB
%token ID
%token IF
%token ELSE
%token WHILE
%token INPUT
%token CONST_SIR
%left NOT
%left AND
%left OR
%token WRITE

%%
    program: lista_instr;

    constanta: CONST_INT
             | CONST_REAL
             | CONST_SIR
             ;

    lista_instr: instr
               | lista_instr NEWLINE instr
               ;

    instr: %empty
         | instr_atrib
         | instr_while
         | instr_write
         | instr_read
         | instr_if
         ;

    instr_atrib: variabila ATRIB expresie
               | variabila ATRIB instr_read
               ;

    variabila: ID;

    expresie: factor
            | expresie '+' expresie
            | expresie '*' expresie
            | expresie '/' expresie
            | expresie '-' expresie
            ;

    factor: ID 
          | constanta
          | ID '(' lista_expr ')'
          | '(' expresie ')'
          ;

    lista_expr: expresie
              | lista_expr ',' expresie
              ;

    instr_if: IF conditie':' NEWLINE instr ramura_else;

    ramura_else: %empty
               | NEWLINE ELSE':' NEWLINE instr
               ;

    conditie: expr_logica
            | expresie op_rel expresie
            ;

    expr_logica: factor_logic  
    		   | expr_logica AND expr_logica 
    		   | expr_logica OR expr_logica
    		   ;

    factor_logic: '(' conditie ')'
                | NOT factor_logic
                ;

    op_rel: '<'
          | '>'
          ;

    instr_while: WHILE conditie':' NEWLINE instr;

    instr_write: WRITE '(' lista_elem ')';

    lista_elem: element
              | lista_elem ',' element
              ;

    element: expresie
           | CONST_SIR
           ;

    instr_read: INPUT '('')';
%%

void yyerror (char *s) {
	fprintf(stderr, ANSI_COLOR_RED "%s on line: %d\n" ANSI_COLOR_RESET, s, yylineno);
    exit(1);
}

extern FILE *yyin;

int main(int argc, char **argv)
{
	if(argc>1) yyin = fopen(argv[1], "r");

	if((argc>2) && (!strcmp(argv[2],"-d"))) yydebug = 1;

	if(!yyparse()) fprintf(stderr, ANSI_COLOR_GREEN "The file is syntactically correct.\n" ANSI_COLOR_RESET);
}
