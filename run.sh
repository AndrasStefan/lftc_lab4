#!/bin/bash

flex flex_python.l
bison -dy bison_python.y --warnings=none

gcc lex.yy.c y.tab.c -o python

# rm lex.yy.c
# rm y.tab.c
# rm y.tab.h

./python < $1
