%{
	#include "y.tab.h"
	void yyerror(char []);
	char buffer[50];
%}

%option noyywrap
%option caseless
%option yylineno

CIFRA_ZEC   	[0-9]
IDENTIFICATOR   [a-z][a-z0-9]*
OPERATOR_1    	"+"|"-"|"*"|"/"|"="|"!="|"<"|">"
DELIMIT_1   	":"|"("|")"
NR_BAZA10   	{CIFRA_ZEC}+
NR_REAL     	{CIFRA_ZEC}+"."{CIFRA_ZEC}*|{CIFRA_ZEC}+
SIR_CAR			["][^\n"]*["]

%%
"#"             {
                    /* skip comment */
                }

\n              { return NEWLINE; }

else            { return ELSE; }
if              { return IF; }
input           { return INPUT; }
print           { return WRITE; }
while           { return WHILE; }
=             	{ return ATRIB; }
or              { return OR; }
and             { return AND; }
not             { return NOT; }
{IDENTIFICATOR} { return ID; }

{NR_BAZA10} { 
				yylval.p_val= yytext;
				return CONST_INT; 
			}

{NR_REAL}   {
				yylval.p_val= yytext;
				return CONST_REAL; 
			}

{SIR_CAR} 		{ return CONST_SIR; }
{DELIMIT_1}     { return yytext[0]; }
{OPERATOR_1}    { return yytext[0]; }
[ \t]+    		{ /* whitespaces */ }

.           {
				sprintf(buffer, "Invalid character '%s'", yytext); 
				yyerror(buffer);
			}
%%
